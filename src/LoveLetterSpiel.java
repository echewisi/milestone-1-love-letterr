import java.util.ArrayList;
import java.util.Scanner;

public class LoveLetterSpiel {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<String> spieler = new ArrayList<>();
        ArrayList<String> spielernamen = new ArrayList<>();
        ArrayList<String> spielerkarten = new ArrayList<>();

        System.out.print("Geben Sie die Anzahl der Spieler ein (2-4): ");
        int spieleranzahl = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < spieleranzahl; i++) {
            System.out.print("Geben Sie den Namen von Spieler " + (i + 1) + " ein: ");
            String spielername = scanner.nextLine();
            spieler.add(spielername);
            spielernamen.add(spielername);
            spielerkarten.add("");
        }

        System.out.println("Spiel gestartet!");

        while (true) {
            System.out.print("Geben Sie einen Befehl ein (geben Sie '\\help' ein, um eine Liste der Befehle anzuzeigen): ");
            String befehl = scanner.nextLine();

            if (befehl.equals("\\help")) {
                System.out.println("Verfügbare Befehle:");
                System.out.println("\\start - Startet das Spiel.");
                System.out.println("\\playCard - Spielt eine Karte.");
                System.out.println("\\showHand - Zeigt die Hand des aktuellen Spielers.");
                System.out.println("\\showScore - Zeigt den Punktestand der Spieler.");
                System.out.println("\\exit - Beendet das Spiel.");
            } else if (befehl.equals("\\start")) {
                System.out.println("Spiel gestartet.");
            } else if (befehl.equals("\\playCard")) {
                System.out.print("Geben Sie Ihren Namen ein: ");
                String spielername = scanner.nextLine();

                if (spielernamen.contains(spielername)) {
                    System.out.print("Geben Sie den Kartennamen ein: ");
                    String kartename = scanner.nextLine();

                    int spielerindex = spielernamen.indexOf(spielername);
                    spielerkarten.set(spielerindex, kartename);

                    System.out.println("Karte gespielt!");
                } else {
                    System.out.println("Spieler nicht gefunden.");
                }
            } else if (befehl.equals("\\showHand")) {
                System.out.print("Geben Sie Ihren Namen ein: ");
                String spielername = scanner.nextLine();

                if (spielernamen.contains(spielername)) {
                    int spielerindex = spielernamen.indexOf(spielername);
                    String hand = spielerkarten.get(spielerindex);
                    System.out.println("Hand von " + spielername + ": " + hand);
                } else {
                    System.out.println("Spieler nicht gefunden.");
                }
            } else if (befehl.equals("\\showScore")) {
                for (int i = 0; i < spieleranzahl; i++) {
                    System.out.println("Punktzahl von " + spielernamen.get(i) + ": " + spielerkarten.get(i).length());
                }
            } else if (befehl.equals("\\exit")) {
                System.out.println("Auf Wiedersehen!");
                break;
            } else {
                System.out.println("Unbekannter Befehl. Geben Sie '\\help' ein, um eine Liste der Befehle anzuzeigen.");
            }
        }
    }
}